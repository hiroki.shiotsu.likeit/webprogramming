<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報更新</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">

</head>
<body>
   <div class="page-header">
       <a href="LogoutServlet">ログアウト</a>
       <span class="user-name">${userInfo.name} さん</span>
   </div>

   <div class = "page-title">
    <h1>ユーザ情報更新</h1>
    </div>




    <div class="user-info">
    	<form action ="UserUpdateServlet" method="post">
			<input type ="hidden" name="id" value="${userData.id}">
            <table class="table table-borderless">
             <thead class="t">
                <tr>
                <th scope="col"></th>
                 <th scope="col"></th>

                </tr>
			             </thead>
              <tr>

                <th scope="row">ログインID</th>
                <td>${userData.loginId}</td>
              </tr>
              <tr>
                <th scope="row">パスワード</th>
                <th><input type="password" name="password1"></th>
              </tr>
            <tr>
                <th scope="row">パスワード(確認)</th>
                <td><input type="password" name="password2"></td>
              </tr>
            <tr>
                <th scope="row">ユーザー名</th>
                <td><input type="text" value="${userData.name}" name="name"></td>
              </tr>
            <tr>
                <th scope="row">生年月日</th>
                <td><input type="date" value="${userData.birthDate}" name="birthDate"></td>
             </tr>



              </table>

              <div class="send">
              <input type="submit" value="更新" style="width:120">
              </div>
             </form>
    </div>

    <div class="back"><a href="UserListServlet">戻る</a></div>
</body>
</html>