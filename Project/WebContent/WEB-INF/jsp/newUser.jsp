<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザー登録ページ</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">

</head>
<body>
   <div class="page-header">
       <a href="LogoutServlet">ログアウト</a>
       <span class="user-name">${userInfo.name} さん</span>
   </div>

   <div class = "page-title">
    <h1>ユーザー新規登録</h1>
    </div>

     <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    <div class="container-fluid">
    <div class="user-info">
           <form action ="NewUserServlet" method="post">

       		<table class="table table-borderless">
             <thead class="t">
                <tr>
                <th scope="col"></th>
                 <th scope="col"></th>

                </tr>



             </thead>
              <tr>
                <td scope="row">ログインID</td>
                <td><input type="text" name="loginId" value= "${loginId}" > </td>
              </tr>
              <tr>
                <td scope="row">パスワード</td>
                <td><input type="password" name="password1"></td>
              </tr>
            <tr>
                <td scope="row">パスワード(確認)</td>
                <td><input type="password" name="password2"></td>
              </tr>
            <tr>
                <td scope="row">ユーザー名</td>
                <td><input type="text" name="name" value="${name}"></td>
              </tr>
            <tr>
                <td scope="row">生年月日</td>
                <td><input type="date" name="birthDate" value="${birthDate}" ></td>
             </tr>



              </table>
       		<div class="send">
              <input type="submit" value="登録" style="width:200">
			</div>

			</form>


       </div>

    </div>





    <div class="back"><a href="UserListServlet">戻る</a></div>






</body>
</html>