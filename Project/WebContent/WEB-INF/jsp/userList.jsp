<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザー登録ページ</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">

</head>
<body>
   <div class="page-header">
       <a href="LogoutServlet">ログアウト</a>
       <span class="user-name">${userInfo.name}さん</span>
   </div>

   <div class = "page-title">
    <h1>ユーザ一覧</h1>
    </div>

    <div class="container-fluid">
      <div class="new-entry"><a href="NewUserServlet">新規登録</a> </div>

	        <div>
        <form action="UserListServlet" method="post">

            <table class="table table-borderless">

              <tr class="d-flex">
                <th class="col-3">ログインID</th>
                <td class="col-9">
                <input type="text" style="width:350px; height: 30px;" name="LoginID"></td>
              </tr>

               <tr class="d-flex">
                <th class="col-3">ユーザ名</th>
                <td class="col-9">
                <input type="text" style="width:350px; height: 30px;" name="Name"></td>
                </tr>

                <tr class="d-flex">
                    <th class="col-sm-3">生年月日</th>
                    <th class="col-sm-9">
                        <input type="date" name="birthDate1">
                        <span>　〜　　</span>
                        <input type="date" style="width:123px" name="birthDate2">
                </th>
                </tr>

            </table>
			<div class="user-search"><input type="submit" value="検索"> </div>
           </form>
            </div>

            </div>




        <div class="user-info">
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr class="d-flex">
                        <th class="col-sm-2">ログインID</th>
                        <th class="col-sm-2">
                            ユーザー名
                        </th>
                        <th class="col-sm-4">
                          生年月日
                        </th>
                        <th class="col-sm-4"></th>
                    </tr>
                </thead>
                <tbody>

                 <c:forEach var="user" items="${userList}" >
                    <tr class="d-flex">
                        <td class="col-sm-2">${user.loginId}</td>
                        <td class="col-sm-2">${user.name}</td>
                        <td class="col-sm-4">${user.birthDate}</td>
                        <td class="col-sm-4">

                      <c:choose>
                       <c:when test="${userInfo.loginId == 'admin'}">

                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                       </c:when>

                       <c:otherwise>

                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <c:if test="${userInfo.loginId == user.loginId}">
						<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>
                       </c:otherwise>


                      </c:choose>

                       </td>
                    </tr>
                    </c:forEach>

                 </tbody>

            </table>
        </div>

</div>








</body>
</html>