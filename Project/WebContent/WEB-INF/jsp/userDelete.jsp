<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザ消去確認</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">

</head>
<body>
   <div class="page-header">
       <a href="LogoutServlet">ログアウト</a>
       <span class="user-name">${userInfo.name}さん</span>
   </div>

   <div class = "page-title">
    <h1>ユーザ消去確認</h1>
    </div>
    <div class="container-fluid">
       <div class="delete-check">
           <span>ログインID: ${userData.loginId}</span>
        <p>本当に消去してよろしいでしょうか</p>
        </div>


        <div class="delete-button">
        <div style="display:inline-flex">
        <form action="UserListServlet" method="get">
        <input type="submit" value="キャンセル">
        </form>
        <form action="UserDeleteServlet" method="post">
        <input type="hidden" value="${userData.id}" name="id">
        <input type="submit" value="OK" >
        </form>

        </div>
        </div>



    </div>



</body>
</html>