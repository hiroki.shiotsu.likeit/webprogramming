<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">
</head>
<body>
   <div class = "page-title">
    <h1>ログイン画面</h1>
    </div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


<form action="LoginServlet" method="post">
  <div class="container-fluid">
        <div class="login-form">
           <span>ログインID</span>
           <input type="text" name="loginId">
        </div>

        <div class="login-form">
           <span>パスワード</span>
           <input type="password" name="password">
        </div>

        <div class="send">
            <input type="submit" value="ログイン">
        </div>

    </div>
</form>

</body>
</html>