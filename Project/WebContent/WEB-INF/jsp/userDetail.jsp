<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報詳細</title>
     <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="css/mock.css" type="text/css">

</head>
<body>
   <div class="page-header">
       <a href="LogoutServlet">ログアウト</a>
       <span class="user-name">${userInfo.name} さん</span>
   </div>

   <div class = "page-title">
    <h1>ユーザ情報詳細参照</h1>
    </div>

    <div class="user-info">
            <table class="table table-borderless">
             <thead >
                <tr>
                <th scope="col-sm-6"></th>
                 <th scope="col-sm-6"></th>

                </tr>

             </thead>
              <tr>
                <th scope="row">ログインID</th>
                <td>${userData.loginId}</td>
              </tr>
              <tr>
                <th scope="row">ユーザ名</th>
                <td>${userData.name}</td>
              </tr>
            <tr>
                <th scope="row">生年月日</th>
                <td>${userData.birthDate}</td>
              </tr>
            <tr>
                <th scope="row">登録日時</th>
                <td>${userData.createDate}</td>
              </tr>
            <tr>
                <th scope="row">更新日時</th>
                <td>${userData.updateDate}</td>
             </tr>



              </table>


              <div class="back"><a href="UserListServlet">戻る</a></div>
        </div>
</body>
</html>