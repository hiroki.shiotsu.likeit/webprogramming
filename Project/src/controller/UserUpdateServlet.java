package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログインセッションがない場合ログインページ
		HttpSession session = request.getSession(false);
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}



		// URLからGETパラメータとしてIDを受け取る
				String id = request.getParameter("id");

				// 確認用：idをコンソールに出力
				System.out.println(id);


				UserDao userDao = new UserDao();
				User userData = userDao.UserDetail(id);





				// TODO  ユーザ情報をリクエストスコープにセットしてjspにフォワード
				request.setAttribute("userData", userData);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String password = null;
		if(password1.equals(password2)) {
			password = password1;
		}

		UserDao userDao = new UserDao();

		if(password1.isEmpty() && password2.isEmpty()) {

			int SQLResult = userDao.UserUpdate(id, name, birthDate);
			if(SQLResult == 0) {
				 request.setAttribute("loginId", id);
				 request.setAttribute("name", name);
				 request.setAttribute("birthDate", birthDate);
				 request.setAttribute("errMsg", "入力された内容は正しくありません");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
					dispatcher.forward(request, response);
					return;
			}


		}else {
			 int SQLResult =  userDao.UserUpdate(id, name, birthDate, password);
			if(SQLResult == 0) {
				 request.setAttribute("loginId", id);
				 request.setAttribute("name", name);
				 request.setAttribute("birthDate", birthDate);
				 request.setAttribute("errMsg", "入力された内容は正しくありません");

				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
					dispatcher.forward(request, response);
					return;
				}
		}

		response.sendRedirect("UserListServlet");
	}

}
