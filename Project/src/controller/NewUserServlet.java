package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;


/**
 * Servlet implementation class NewUserServlet
 */
@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// ユーザ一覧情報を取得
				UserDao userDao = new UserDao();
				List<User> userList = userDao.findAll();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userList);

				// ユーザ一覧のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザー新規登録に戻る 入力した情報をSQLにセットし登録、失敗した場合はパスワードを空欄に
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String password = null;
		if(loginId.isEmpty()) {
			loginId = null;
		}

		if(password1.equals(password2) ) {
			password = password1;
		}

		if(password1.isEmpty() || password2.isEmpty()) {
			 request.setAttribute("loginId", loginId);
			 request.setAttribute("name", name);
			 request.setAttribute("birthDate", birthDate);
			 request.setAttribute("errMsg", "入力された内容は正しくありません");

			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		int SQLResult =  userDao.AddUser(loginId, name, birthDate, password);
		 if(SQLResult == 0) {
			 request.setAttribute("loginId", loginId);
			 request.setAttribute("name", name);
			 request.setAttribute("birthDate", birthDate);
			 request.setAttribute("errMsg", "入力された内容は正しくありません");

			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;
			}

		 response.sendRedirect("UserListServlet");



	}

}
