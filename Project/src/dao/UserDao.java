package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	public String passEncrypt(String password) {
		//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			return result;
		}


    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        String pass = this.passEncrypt(password);
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, pass);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {

                int id = rs.getInt("id");
                if(id == 1) {
                	continue;
                }
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    //ユーザー取得Dao
    public User UserDetail(String Id) {
    	Connection conn = null;
    	PreparedStatement pStmt = null;

    		try {
    		//データベース接続
    			conn = DBManager.getConnection();

    			//Select文
    			String sql = "SELECT * FROM user WHERE id = ?";

    			//ステートメントの作成
    			pStmt = conn.prepareStatement(sql);
    			pStmt.setString(1, Id);
    			ResultSet rs = pStmt.executeQuery();

    			 if (!rs.next()) {
    	                return null;
    	            }

    			 int id = rs.getInt("id");
    			String loginId = rs.getString("login_id");
    			String nameData = rs.getString("name");
    			Date birthDate = rs.getDate("birth_date");
    			String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
    			return new User(id,loginId,nameData,birthDate,createDate,updateDate);

    		}catch(SQLException e){
    			e.printStackTrace();
                return null;
    		}finally {
    			 if (conn != null) {
    	                try {
    	                    conn.close();
    	                } catch (SQLException e) {
    	                    e.printStackTrace();
    	                    return null;
    	                }
    	            }
    		}
    }
    //データ消去Dao
    public int UserDelete(String id) {
    	Connection conn = null;
    	PreparedStatement pStmt = null;
    	int success = 1;
    	int failed = 0;

    	try {
    		//接続
    		conn = DBManager.getConnection();

    		//delete文の準備
    		String sql = "DELETE FROM user WHERE id = ?";
    		pStmt = conn.prepareStatement(sql);
    		//セット
    		pStmt.setString(1, id);

   		 	int result = pStmt.executeUpdate();
   		 	if(result > 0) {
   		 		return success;
   		 	}else {
   		 		//失敗した場合
   		 		return failed;
   		 	}

    	}catch(SQLException e){
    		e.printStackTrace();
    		return failed;
    	}finally {
    		if(conn != null) {
    			try {
    				conn.close();

    			}catch(SQLException e){
    				e.printStackTrace();
    				return failed;
    				}
    			}
    	}

    }





    //データ更新Dao
    public int UserUpdate(String id,String name,String birthDate,String password ){
    	Connection conn = null;
   	 	PreparedStatement pStmt = null;
   	 	int success = 1;
   	 	int failed = 0;

   	 	//暗号化処理
   	 	String pass = this.passEncrypt(password);


   	 try {
		 //データベースへ接続
		 conn = DBManager.getConnection();

		 //insert文の準備
		 String sql = "UPDATE user SET name = ?,birth_date = ?,password = ? WHERE id = ? ";


		 //ステートメントの生成
		 pStmt = conn.prepareStatement(sql);
		 //SQLの?にパラメータの設定
		 pStmt.setString(1, name);
         pStmt.setString(2,birthDate);
         pStmt.setString(3,pass);
         pStmt.setString(4,id);

         //実行
		 int SQLResult = pStmt.executeUpdate();
		 //データ登録の成功の判別
		 if(SQLResult > 0) {
			 return success;
		 }else {
			return failed;
		 }

	 }catch(SQLException e){
		 e.printStackTrace();
		 return failed;
}finally {
		if(conn != null) {
			try {
				conn.close();

			}catch(SQLException e){
				e.printStackTrace();
				return failed;
				}
			}

}

    }


    //パスワードが空欄の場合のデータ更新Dao
    public int UserUpdate(String id,String name,String birthDate){
    	Connection conn = null;
   	 	PreparedStatement pStmt = null;
   	 	int success = 1;
   	 	int failed = 0;

   	 try {
		 //データベースへ接続
		 conn = DBManager.getConnection();

		 //insert文の準備
		 String sql = "UPDATE user SET name = ?,birth_date = ? WHERE id = ? ";


		 //ステートメントの生成
		 pStmt = conn.prepareStatement(sql);
		 //SQLの?にパラメータの設定
		 pStmt.setString(1, name);
         pStmt.setString(2,birthDate);
         pStmt.setString(3,id);

         //実行
		 int result = pStmt.executeUpdate();
		 //データ登録の成功の判別
		 if(result > 0) {
			 return success;
		 }else {
			return failed;
		 }

	 }catch(SQLException e){
		 e.printStackTrace();
		 return failed;
}finally {
		if(conn != null) {
			try {
				conn.close();

			}catch(SQLException e){
				e.printStackTrace();
				return failed;
				}
			}

}

    }




    //新規登録のDao
    public int  AddUser(String loginId,String name,String birthDate,String password) {
    	 Connection conn = null;
    	 PreparedStatement pStmt = null;
    	 int success = 1;
    	 int failed = 0;
    	 String pass = this.passEncrypt(password);
    	 try {
    		 //データベースへ接続
    		 conn = DBManager.getConnection();

    		 //insert文の準備
    		 String sql = "INSERT INTO user (login_id,name,birth_date,password) VALUES (?,?,?,?)";


    		 //ステートメントの生成
    		 pStmt = conn.prepareStatement(sql);
    		 //SQLの?にパラメータの設定
    		 pStmt.setString(1, loginId);
             pStmt.setString(2,name);
             pStmt.setString(3,birthDate);
             pStmt.setString(4,pass);

             //実行
    		 int result = pStmt.executeUpdate();
    		 //データ登録の成功の判別
    		 if(result > 0) {
    			 return success;
    		 }else {
    			return failed;
    		 }

    	 }catch(SQLException e){
    		 e.printStackTrace();
    		 return failed;
    }finally {
    		if(conn != null) {
    			try {
    				conn.close();

    			}catch(SQLException e){
    				e.printStackTrace();
    				return failed;
    				}
    			}

    }


    }

    //ユーザー検索dao
    public List<User> SearchUser(String LoginID,String Name,String birthDate1,String birthDate2){
    	 Connection conn = null;
         List<User> userList = new ArrayList<User>();
         String sql = null;
         PreparedStatement pStmt = null;

         try {
             // データベースへ接続
             conn = DBManager.getConnection();

             // SELECT文を分岐
             if(Name.equals("") && birthDate1.equals("") && birthDate2.equals("")) {
            	 	sql = "SELECT * FROM user WHERE login_id = ?";
            	 	pStmt = conn.prepareStatement(sql);
            	 	pStmt.setString(1, LoginID);
            	 }else if(LoginID.equals("") && birthDate1.equals("") && birthDate2.equals("")) {
                 	System.out.println(3);

                 	sql="SELECT * FROM user WHERE name LIKE ?";
                 	pStmt = conn.prepareStatement(sql);
                     pStmt.setString(1, "%" + Name + "%");
                 }else if(LoginID.equals("") && Name.equals("") && birthDate1.equals("")) {
                 	System.out.println(5);

                 	sql="SELECT * FROM user WHERE birth_date < ?";
                 	pStmt = conn.prepareStatement(sql);
                     pStmt.setString(1, birthDate2);

                 }else if(LoginID.equals("") && Name.equals("") && birthDate2.equals("")) {
                 	System.out.println(6);

                 	sql="SELECT * FROM user WHERE birth_date > ?";
                 	pStmt = conn.prepareStatement(sql);
                     pStmt.setString(1, birthDate1);
                 }else if(birthDate1.equals("") && birthDate2.equals("")) {
            		 sql = "SELECT * FROM user WHERE login_id = ? AND name LIKE ?";
            		pStmt = conn.prepareStatement(sql);
           	 		pStmt.setString(1, LoginID);
           	 		pStmt.setString(2,"%" + Name + "%");
            	 }else if(Name.equals("") && birthDate1.equals("")) {
                	sql = "SELECT * FROM user WHERE login_id = ? AND birth_date < ? ";
                	pStmt = conn.prepareStatement(sql);
                	pStmt.setString(1, LoginID);
                	pStmt.setString(2, birthDate2);
                }else if(Name.equals("") && birthDate2.equals("")) {
                	sql = "SELECT * FROM user WHERE login_id = ? AND birth_date < ? ";
                    pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1, LoginID);
                    pStmt.setString(2, birthDate1);
                }else if(LoginID.equals("") && birthDate1.equals("")){
                	System.out.println(1);
                	sql="SELECT * FROM user WHERE name LIKE ? AND birth_date < ?";
                	pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1, "%" + Name + "%");
                    pStmt.setString(2, birthDate2);
                }else if(LoginID.equals("") && birthDate2.equals("")) {
                	System.out.println(2);

                	sql="SELECT * FROM user WHERE name LIKE ? AND birth_date > ?";
                	pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1, "%" + Name + "%");
                    pStmt.setString(2, birthDate1);
                }else if(LoginID.equals("") && Name.equals("")) {
                	System.out.println(4);

                	sql="SELECT * FROM user WHERE birth_date BETWEEN ? AND ?";
                	pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1, birthDate1);
                    pStmt.setString(2, birthDate2);
                }else if(LoginID.equals("")){
                	System.out.print(5);
                	sql="SELECT * FROM user WHERE name LIKE ? AND birth_date BETWEEN ? AND ?";
                	pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1,"%" + Name + "%");
                    pStmt.setString(2, birthDate1);
                    pStmt.setString(3, birthDate2);
                }else if(Name.equals("")) {
                	sql="SELECT * FROM user WHERE login_id = ? AND birth_date BETWEEN ? AND ?";
                	pStmt = conn.prepareStatement(sql);
                    pStmt.setString(1, LoginID);
                    pStmt.setString(2, birthDate1);
                    pStmt.setString(3, birthDate2);
                }else if(birthDate1.equals("")) {
           		 sql = "SELECT * FROM user WHERE login_id = ? AND name LIKE ? AND birth_date < ? ";
           		pStmt = conn.prepareStatement(sql);
          	 		pStmt.setString(1, LoginID);
          	 		pStmt.setString(2,"%" + Name + "%");
          	 		pStmt.setString(3, birthDate2);
           	 	}else if(birthDate2.equals("")) {
        		 sql = "SELECT * FROM user WHERE login_id = ? AND name LIKE ? AND birth_date > ? ";
        		 pStmt = conn.prepareStatement(sql);
        		 pStmt.setString(1, LoginID);
        		 pStmt.setString(2,"%" + Name + "%");
        		 pStmt.setString(3, birthDate1);
           	 	}else{
                	System.out.println(13);
                	sql = "SELECT * FROM user WHERE  login_id = ? AND name LIKE ? AND birth_date BETWEEN ? AND ? ";
                	pStmt = conn.prepareStatement(sql);
         			pStmt.setString(1, LoginID);
         			pStmt.setString(2,"%" + Name + "%");
         			pStmt.setString(3, birthDate1);
         			pStmt.setString(4, birthDate2);
                }




              // SELECTを実行し、結果表を取得

               ResultSet rs = pStmt.executeQuery();



             // 結果表に格納されたレコードの内容を
             // Userインスタンスに設定し、ArrayListインスタンスに追加
             while (rs.next()) {
                 int id = rs.getInt("id");
                 if(id == 1) {
                	 continue;
                 }
                 String loginId = rs.getString("login_id");
                 String name = rs.getString("name");
                 Date birthDate = rs.getDate("birth_date");
                 String password = rs.getString("password");
                 String createDate = rs.getString("create_date");
                 String updateDate = rs.getString("update_date");
                 User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                 userList.add(user);
             }
         } catch (SQLException e) {
             e.printStackTrace();
             return null;
         } finally {
             // データベース切断
             if (conn != null) {
                 try {
                     conn.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                     return null;
                 }
             }
         }
         return userList;



    }


   }



